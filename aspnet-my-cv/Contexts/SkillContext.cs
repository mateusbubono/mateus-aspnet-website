using MySql.Data.MySqlClient;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using aspnet_my_cv.Exceptions;
using aspnet_my_cv.Models;
using aspnet_my_cv.Contexts;

namespace aspnet_my_cv.Contexts;

public class SkillContext
{
    public string ConnectionString { get; set; }

    public SkillContext(string connectionString)
    {
        this.ConnectionString = connectionString;
    }

    public SkillContext()
    {
    }

    private MySqlConnection GetConnection()
    {
        return new MySqlConnection(ConnectionString);
    }

    public List<Skill> GetSkillsByCVId(int id, MySqlConnection conn)
    {
        List<Skill> skills = new List<Skill>();
        
        MySqlCommand cmd = new MySqlCommand(
"SELECT Competence.id, Competence.competence FROM CV_Competence INNER JOIN Competence ON CV_Competence.competence_id=id WHERE CV_Competence.cv_id=@ID", conn);
        cmd.Parameters.Add("@ID", MySqlDbType.Int32);
        cmd.Parameters["@ID"].Value = id;
        
        using(var reader = cmd.ExecuteReader())
        {
            while(reader.Read() == true)
            {
                Skill skill = new Skill();
                skill.Id = Convert.ToInt32(reader["id"]);
                skill.Description = reader["competence"].ToString();

                skills.Add(skill);
            }
        }

        return skills;

    }
    
}