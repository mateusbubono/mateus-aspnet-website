using MySql.Data.MySqlClient;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using aspnet_my_cv.Exceptions;
using aspnet_my_cv.Models;
using aspnet_my_cv.Contexts;

namespace aspnet_my_cv.Contexts;

public class CVContext
{
    public string ConnectionString { get; set; }

    public CVContext(string connectionString)
    {
        this.ConnectionString = connectionString;
    }

    private MySqlConnection GetConnection()
    {
        return new MySqlConnection(ConnectionString);
    }

    public CV GetCVById(int id)
    {
        CV cv = new CV();
        using(MySqlConnection conn = GetConnection())
        {
            conn.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM CV WHERE id=@ID", conn);
            cmd.Parameters.Add("@ID", MySqlDbType.Int32);
            cmd.Parameters["@ID"].Value = id;


            using(var reader = cmd.ExecuteReader())
            {
                if(reader.Read() == true)
                {
                    cv.Id = Convert.ToInt32(reader["id"]);
                    cv.Title = reader["titre"].ToString();
                    cv.SubTitle = reader["sous_titre"].ToString();
                    cv.EntryName = reader["nom_enregistrement"].ToString();
                }
            }

            GetHobbies(cv, conn);
            GetSkills(cv, conn);
            GetUserInformation(cv, conn);
            GetFormation(cv, conn);
            GetExperiances(cv, conn);
            
        }

        if(CheckCV(cv) == false)
            throw new CVNotFoundException("CV a l'id : "+id+" non trouvé");

        return cv;
    }

    private bool CheckCV(CV cv)
    {
        return cv.Id > 0 && cv.Title  != null &&
        cv.SubTitle != null && cv.EntryName !=null;
    }

    private void GetHobbies(CV cv, MySqlConnection conn)
    {
        HobbiesContext hobbiesContext = new HobbiesContext();

        cv.Hobbie = hobbiesContext.GetHobbiesByCVId(cv.Id, conn);
    }

    private void GetSkills(CV cv, MySqlConnection conn)
    {
        SkillContext skillContext = new SkillContext();

        cv.Skills = skillContext.GetSkillsByCVId(cv.Id, conn);
    }

    private void GetUserInformation(CV cv, MySqlConnection conn)
    { 
        UserInformationContext userInformationContext = new UserInformationContext();

        cv.UserInfo = userInformationContext.GetUserInformationByCVId(cv.Id, conn);
    }

    private void GetFormation(CV cv, MySqlConnection conn)
    {
        FormationContext formationContext = new FormationContext();

        cv.Formations = formationContext.GetFormationContextByCVId(cv.Id, conn);
    }

    private void GetExperiances(CV cv, MySqlConnection conn)
    {
        ExperianceContext experianceContext = new ExperianceContext();

        cv.Experiances = experianceContext.GetExperiancesByCVId(cv.Id, conn);
    }

}