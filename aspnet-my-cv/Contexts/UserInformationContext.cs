using MySql.Data.MySqlClient;
using System;
using aspnet_my_cv.Models;

namespace aspnet_my_cv.Contexts;
public class UserInformationContext
{
    public string ConnectionString { get; set; }

    public UserInformationContext(string connectionString)
    {
        this.ConnectionString = connectionString;
    }

    public UserInformationContext()
    {
    }

    private MySqlConnection GetConnection()
    {
        return new MySqlConnection(ConnectionString);
    }

    public UserInformation GetUserInformationByCVId(int cvId, MySqlConnection conn)
    {
        UserInformation userInformation = new UserInformation();
        
        MySqlCommand cmd = new MySqlCommand("SELECT Coordonnees.* FROM CV_Coordonnees INNER JOIN Coordonnees ON CV_Coordonnees.coordonnees_id=Coordonnees.id WHERE cv_id=@ID", conn);
        cmd.Parameters.Add("@ID", MySqlDbType.Int32);
        cmd.Parameters["@ID"].Value = cvId;

        using (var reader = cmd.ExecuteReader())
        {
            if (reader.Read())
            {
                userInformation.Id = Convert.ToInt32(reader["id"]);
                userInformation.PhoneNumber = reader["numero_telephone"].ToString();
                userInformation.Email = reader["email"].ToString();
                userInformation.Address = reader["adresse"].ToString();
                userInformation.PortfolioURL = reader["portfolio_url"].ToString();
                userInformation.Nationality = reader["nationalite"].ToString();
            }
        }

            return userInformation;
    }
}
