using MySql.Data.MySqlClient;
using System;
using aspnet_my_cv.Models;

namespace aspnet_my_cv.Contexts;
public class ExperianceContext
{
    public string ConnectionString { get; set; }

    public ExperianceContext(string connectionString)
    {
        this.ConnectionString = connectionString;
    }

    public ExperianceContext()
    {
    }

    private MySqlConnection GetConnection()
    {
        return new MySqlConnection(ConnectionString);
    }

    public List<Experiance> GetExperiancesByCVId(int cvId, MySqlConnection conn)
    {
        List<Experiance> experiances = new List<Experiance>();
        
        MySqlCommand cmd = new MySqlCommand("SELECT Experience.* FROM CV_Experience INNER JOIN Experience ON CV_Experience.experience_id=Experience.id WHERE cv_id=@ID", conn);
        cmd.Parameters.Add("@ID", MySqlDbType.Int32);
        cmd.Parameters["@ID"].Value = cvId;

        using (var reader = cmd.ExecuteReader())
        {
            while(reader.Read())
            {
                Experiance experiance = new Experiance();
                experiance.Id = Convert.ToInt32(reader["id"]);
                experiance.CompanyAndJob = reader["entreprise_et_poste"].ToString();
                experiance.Period = reader["periode_travail"].ToString();
                experiances.Add(experiance); 
            }
        }

            return experiances;
    }
}
