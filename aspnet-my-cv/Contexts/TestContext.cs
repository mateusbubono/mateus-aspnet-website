using MySql.Data.MySqlClient;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;


namespace aspnet_my_cv.Contexts;

public class TestContext
{
    public string ConnectionString { get; set; }

    public TestContext(string connectionString)
    {
        this.ConnectionString = connectionString;
    }

    private MySqlConnection GetConnection()
    {
        return new MySqlConnection(ConnectionString);
    }

    public bool TestConnexionToDB()
    {
        bool result = false;
        using(MySqlConnection conn = GetConnection())
        {
            conn.Open();
            result = true;
        }

        return result;
    }

}

