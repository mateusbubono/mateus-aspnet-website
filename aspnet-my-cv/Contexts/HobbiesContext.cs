using MySql.Data.MySqlClient;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using aspnet_my_cv.Exceptions;
using aspnet_my_cv.Models;
using aspnet_my_cv.Contexts;

namespace aspnet_my_cv.Contexts;

public class HobbiesContext
{
    public string ConnectionString { get; set; }

    public HobbiesContext(string connectionString)
    {
        this.ConnectionString = connectionString;
    }

    public HobbiesContext()
    {
    }

    private MySqlConnection GetConnection()
    {
        return new MySqlConnection(ConnectionString);
    }

    public Hobbies GetHobbiesByCVId(int id, MySqlConnection conn)
    {
        Hobbies hobbies = new Hobbies();
        
        MySqlCommand cmd = new MySqlCommand(
"SELECT Divertissement.id, Divertissement.loisirs FROM CV_Divertissement INNER JOIN Divertissement ON CV_Divertissement.divertissement_id=id WHERE CV_Divertissement.cv_id=@ID", conn);
        cmd.Parameters.Add("@ID", MySqlDbType.Int32);
        cmd.Parameters["@ID"].Value = id;
        
        using(var reader = cmd.ExecuteReader())
        {
            if(reader.Read() == true)
            {
                hobbies.Id = Convert.ToInt32(reader["id"]);
                hobbies.Description = reader["loisirs"].ToString();
            }
        }

        hobbies = InitEmptyHobbies(hobbies);

        return hobbies;

    }

    private Hobbies InitEmptyHobbies(Hobbies hobbies)
    {
        if(hobbies.Id == 0)
        {
            hobbies.Description="";
        }

        return hobbies;
    }

    
}