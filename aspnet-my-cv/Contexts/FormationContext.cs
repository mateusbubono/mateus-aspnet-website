using MySql.Data.MySqlClient;
using System;
using aspnet_my_cv.Models;

namespace aspnet_my_cv.Contexts;
public class FormationContext
{
    public string ConnectionString { get; set; }

    public FormationContext(string connectionString)
    {
        this.ConnectionString = connectionString;
    }

    public FormationContext()
    {
    }

    private MySqlConnection GetConnection()
    {
        return new MySqlConnection(ConnectionString);
    }

    public List<Formation> GetFormationContextByCVId(int cvId, MySqlConnection conn)
    {
        List<Formation> formations = new List<Formation>();
        
        MySqlCommand cmd = new MySqlCommand("SELECT Formation.* FROM CV_Formation INNER JOIN Formation ON CV_Formation.formation_id=Formation.id WHERE cv_id=@ID", conn);
        cmd.Parameters.Add("@ID", MySqlDbType.Int32);
        cmd.Parameters["@ID"].Value = cvId;

        using (var reader = cmd.ExecuteReader())
        {
            while(reader.Read())
            {
                Formation formation = new Formation();
                formation.Id = Convert.ToInt32(reader["id"]);
                formation.University = reader["universite"].ToString();
                formation.Period = reader["periode_enseignement"].ToString();
                formation.Diploma = reader["diplome_prepare"].ToString();

                formations.Add(formation);
                
            }
        }

            return formations;
    }
}
