namespace aspnet_my_cv.Models;

public class CV
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string SubTitle { get; set; }

    public string EntryName { get; set; }

    public Hobbies Hobbie {get; set;}

    public UserInformation UserInfo { get; set; }

    public List<Skill> Skills{get; set;}

    public List<Formation> Formations{get; set;}

    public List<Experiance> Experiances{get; set;}

 }
