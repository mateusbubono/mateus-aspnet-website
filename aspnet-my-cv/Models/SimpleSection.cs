namespace aspnet_my_cv.Models;

public class SimpleSection
{
    public int Id { get; set; }
    public string Description { get; set; }

}
