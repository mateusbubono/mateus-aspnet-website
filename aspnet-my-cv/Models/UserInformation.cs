namespace aspnet_my_cv.Models;

public class UserInformation
{

    public UserInformation()
    {
        Id = 0;
        PhoneNumber = "";
        Email = "";
        Address = "";
        PortfolioURL = "";
        Nationality = "";
    }

    public int Id;
    public string PhoneNumber { get; set; }
    public string Email { get; set; }

    public string Address {get; set;}

    public string PortfolioURL {get; set;}

    public string Nationality {get; set;}

}