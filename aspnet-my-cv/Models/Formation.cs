namespace aspnet_my_cv.Models;

public class Formation
{

    public Formation()
    {
        Id = 0;
        University = "";
        Period = "";
        Diploma = "";
    }

    public int Id { get; set; }
    public string University { get; set; }
    public string Period { get; set; }

    public string Diploma {get; set;}

}