using NUnit.Framework;
using aspnet_my_cv.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using aspnet_my_cv.Models;
using aspnet_my_cv.Exceptions;
using aspnet_my_cv.Contexts;

namespace aspnet_my_cvTest;
[TestFixture]
public class TestCVContext
{
    Microsoft.AspNetCore.Builder.WebApplicationBuilder builder;
    string connectionString;


    [SetUp]
    public void Setup()
    {
        string[] args = {""};
        builder = WebApplication.CreateBuilder(args);

        connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
    }

    [Test]
    public void Test_GetCVById()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(3);

        Assert.IsNotNull(cv);
        Assert.AreEqual(cv.Id, 3);
        Assert.AreEqual(cv.Title, "John Doe");
        Assert.AreEqual(cv.SubTitle, "Developpeur FullStack");
        Assert.AreEqual(cv.EntryName, "CV Exemple");
    }

    [TestCase(0)]
    [TestCase(-1)]
    public void Test_GetNotExistingCV(int id)
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CVNotFoundException ex = Assert.Throws<CVNotFoundException>(() =>
        {
            CV cv = context.GetCVById(id);
        });

        Assert.AreEqual("CV a l'id : "+id+" non trouvé", ex.Message);
    }

    [Test]
    public void Test_GetCVById_WithHobbies()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(3);

        Assert.IsNotNull(cv.Hobbie);
        Assert.AreEqual(cv.Hobbie.Id, 2); 
        Assert.AreEqual(cv.Hobbie.Description, "Lecture");
    }

    [Test]
    public void Test_GetCVById_WithNoHobbies()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(4); 

        Assert.IsNotNull(cv.Hobbie);
        Assert.AreEqual(cv.Hobbie.Id, 0); 
        Assert.AreEqual(cv.Hobbie.Description, ""); 
    }

    [Test]
    public void Test_GetCompetencesByCVId()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(3);

        Assert.IsNotNull(cv.Skills);

        Assert.IsTrue(cv.Skills.Any(c => c.Description == "C#/ASP.NET"));

        Assert.IsTrue(cv.Skills.Any(c => c.Description == "JavaScript/React"));

        Assert.IsTrue(cv.Skills.Any(c => c.Description == "PHP/Symphony"));

        Assert.IsTrue(cv.Skills.Any(c => c.Description == "C#/Unity3D"));

        Assert.IsTrue(cv.Skills.Any(c => c.Description == "MySQL"));

        Assert.IsTrue(cv.Skills.Any(c => c.Description == "Git"));
    }

    [Test]
    public void Test_GetSkillsByCVId_NoSkills()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(4);

        Assert.IsNotNull(cv.Skills);
        Assert.AreEqual(cv.Skills.Count, 0);
    }

    [Test]
    public void Test_GetUserInformationByCVId()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(3);

        Assert.IsNotNull(cv.UserInfo);
        Assert.AreEqual(cv.UserInfo.Id, 1);
        Assert.AreEqual(cv.UserInfo.PhoneNumber, "0450354248");
        Assert.AreEqual(cv.UserInfo.Email, "john.doe@gmail.com");
        Assert.AreEqual(cv.UserInfo.Address, "51 rue des champignons 17880 Nowhere France");
        Assert.AreEqual(cv.UserInfo.PortfolioURL, "www.portfoliotest.com");
        Assert.AreEqual(cv.UserInfo.Nationality, "Francais");
    }

    [Test]
    public void Test_GetEmptyUserInformationByCVId()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(4);

        Assert.IsNotNull(cv.UserInfo);
        Assert.AreEqual(cv.UserInfo.Id, 0);
        Assert.AreEqual(cv.UserInfo.PhoneNumber, "");
        Assert.AreEqual(cv.UserInfo.Email, "");
        Assert.AreEqual(cv.UserInfo.Address, "");
        Assert.AreEqual(cv.UserInfo.PortfolioURL, "");
        Assert.AreEqual(cv.UserInfo.Nationality, "");
    }

    [Test]
    public void Test_GetFormationByCVId()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(3);

        Assert.IsNotNull(cv.Formations);

        Assert.IsTrue(cv.Formations.Any((f) => {
            return f.University == "Universite de Franche-Comte : CTU formation a distance" && f.Period == "2020-2024" && f.Diploma == "Master Informatique Avancee et Applications : en cours";
        } ));

        Assert.IsTrue(cv.Formations.Any((f) => {
            return f.University == "Universite de Paris VIII : IED Formation a distance" && f.Period == "2013-2018" && f.Diploma == "Licence Informatique : diplome avec mention tres bien";
        } ));

        Assert.IsTrue(cv.Formations.Any((f) => {
            return f.University == "Haute ecole de sante de Geneve" && f.Period == "2006-2010" && f.Diploma == "Bachelor de technique en radiologie medicale : diplome";
        } ));
    }

     [Test]
    public void Test_GetEmptyFormationByCVId()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(4);

        Assert.IsNotNull(cv.Formations);

        Assert.AreEqual(cv.Formations.Count, 0);
    }

    [Test]
    public void Test_GetExperiencesByCVId()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(3);

        Assert.IsNotNull(cv.Experiances);

        Assert.IsTrue(cv.Experiances.Any((e) => {
            return e.CompanyAndJob == "Alpha3i : Developpeur MES" && e.Period == "Mai 2023 - Aujourd'hui";
        }));

        Assert.IsTrue(cv.Experiances.Any((e) => {
            return e.CompanyAndJob == "gaea21 : Developpeur Fullstack" && e.Period == "Avril 2022 - Decembre 2022";
        }));

    }

    [Test]
    public void Test_GetEmptyExperiencesByCVId()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(4);

        Assert.IsNotNull(cv.Experiances);

        Assert.AreEqual(cv.Experiances.Count, 0);

    }

    [Test]
    public void Test_GetExperienceDetailsByCVId()
    {
        aspnet_my_cv.Contexts.CVContext context = new aspnet_my_cv.Contexts.CVContext(connectionString);

        CV cv = context.GetCVById(3);

        Assert.IsNotNull(cv.Experiences);

        foreach (var experience in cv.Experiences)
        {
            var details = context.GetExperienceDetailsByExperienceId(experience.Id);

            Assert.IsNotNull(details);
            Assert.AreEqual(details.ExperienceId, experience.Id);
            // Assurez-vous d'ajouter d'autres assertions pour vérifier les détails, par exemple :
            // Assert.AreEqual(details.Description, "Description de l'expérience");
            // Assert.AreEqual(details.OtherProperty, "Autre propriété");
        }
    }
}