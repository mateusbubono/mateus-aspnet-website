
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();

// Configuration de la connexion à la base de données
var connectionString = "DefaultConnection"; // Remplacez par votre propre chaîne de connexion

// Ajouter des services à l'injection de dépendances
//builder.Services.AddDbContext<TestContext>(options =>
//    options.UseYourDatabaseProvider(connectionString));

builder.Services.Add(new ServiceDescriptor(typeof(aspnet_my_cv.Contexts.TestContext), new aspnet_my_cv.Contexts.TestContext(builder.Configuration.GetConnectionString("DefaultConnection"))));
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html");;

app.Run();
