using System;

namespace aspnet_my_cv.Exceptions;
public class CVNotFoundException : Exception
{
    public CVNotFoundException() : base("CV not found !!!")
    {
    }

    public CVNotFoundException(string message) : base(message)
    {
    }

    public CVNotFoundException(string message, Exception innerException) : base(message, innerException)
    {
    }
}
